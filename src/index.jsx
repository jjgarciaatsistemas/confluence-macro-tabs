import ForgeUI, {
  render,
  Macro,
  MacroConfig,
  Text,
  TextArea,
  useConfig,
  Tabs, 
  Tab, 
} from '@forge/ui';

const defaultConfig = {
	tab1Label: "",
	tab1Text: "",
	tab2Label: "",
	tab2Text: "",
	tab3Label: "",
	tab3Text: ""
};

const App = () => {
  const config = useConfig() || defaultConfig;	
  
  return (
	<Tabs>
		<Tab label={config.tab1Label}>
			<Text>{config.tab1Text}</Text>
		</Tab>
		<Tab label={config.tab2Label}>
			<Text>{config.tab2Text}</Text>
		</Tab>
		<Tab label={config.tab3Label}>
			<Text>{config.tab3Text}</Text>
		</Tab>
	</Tabs>
  );
};

export const run = render(
  <Macro
    app={<App />}
  />
);
// Function that defines the configuration UI
const Config = () => {
  return (
    <MacroConfig>
      <TextArea name="tab1Label" label="Tab 1 Label" defaultValue={defaultConfig.tab1Label} />
      <TextArea name="tab1Text" label="Tab 1 Text" defaultValue={defaultConfig.tab1Text} />
	  <TextArea name="tab2Label" label="Tab 2 Label" defaultValue={defaultConfig.tab2Label} />
      <TextArea name="tab2Text" label="Tab 2 Text" defaultValue={defaultConfig.tab2Text} />
      <TextArea name="tab3Label" label="Tab 3 Label" defaultValue={defaultConfig.tab3Label} />
      <TextArea name="tab3Text" label="Tab 3 Text" defaultValue={defaultConfig.tab3Text} />	 
    </MacroConfig>
  );
};

export const config = render(<Config />);